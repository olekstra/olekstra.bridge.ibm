﻿namespace Olekstra.Bridge.Ibm
{
    using System;
    using Xunit;

    public class DailyTransactions01Tests
    {
        [Fact]
        public void TD_111_20110614()
        {
            var elm = SerializationHelper.EL.Deserialize<DailyTransactions01>(ResourceHelper.GetSampleEL("TD_111_20110614.xml"));

            Assert.NotNull(elm);

            Assert.Equal(Version01.Value, elm.Version);
            Assert.Equal("111", elm.PharmacyChainCode);

            Assert.Equal(2, elm.Document.Length);

            Assert.Equal("36N85", elm.Document[0].DocNumber);
            Assert.Equal(2, elm.Document[0].CountLines);
            Assert.Equal(2, elm.Document[0].Lines.Length);

            var line = elm.Document[0].Lines[0];
            Assert.Equal("1", line.Line);
            Assert.Equal("1111111111", line.PharmacyInn);
            Assert.Equal("15", line.PharmacyCode);
            Assert.Equal("6102366430012435579", line.CardNumber);
            Assert.Equal("14062011", line.OperDate);
            Assert.Equal(new DateTime(2011, 6, 14), line.OperDateValue);
            Assert.Equal("K080601911", line.CashRegisterNumber);
            Assert.Equal("346", line.ShiftNumber);
            Assert.Equal("5413973262316", line.ProductCode);
            Assert.Equal(1, line.Quantity);
            Assert.Equal(839.54M, line.AmountTotal);
            Assert.Equal(359.81M, line.Discount);
            Assert.Equal(1199.35M, line.Price);

            Assert.Equal("36N786", elm.Document[1].DocNumber);
            Assert.Equal(1, elm.Document[1].CountLines);
            Assert.Single(elm.Document[1].Lines);

            line = elm.Document[1].Lines[0];
            Assert.Equal("1", line.Line);
            Assert.Equal("1111111112", line.PharmacyInn);
            Assert.Equal("16", line.PharmacyCode);
            Assert.Equal("6102366430022924109", line.CardNumber);
            Assert.Equal("14062011", line.OperDate);
            Assert.Equal(new DateTime(2011, 6, 14), line.OperDateValue);
            Assert.Equal("K063602951", line.CashRegisterNumber);
            Assert.Equal("78", line.ShiftNumber);
            Assert.Equal("5413973112314", line.ProductCode);
            Assert.Equal(1, line.Quantity);
            Assert.Equal(504.04M, line.AmountTotal);
            Assert.Equal(126.01M, line.Discount);
            Assert.Equal(630.05M, line.Price);
        }
    }
}
