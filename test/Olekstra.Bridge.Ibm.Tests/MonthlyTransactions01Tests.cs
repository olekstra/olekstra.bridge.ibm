﻿namespace Olekstra.Bridge.Ibm
{
    using System;
    using Xunit;

    public class MonthlyTransactions01Tests
    {
        [Fact]
        public void TT_111_201104()
        {
            var elm = SerializationHelper.EL.Deserialize<MonthlyTransactions01>(ResourceHelper.GetSampleEL("TT_111_201104.xml"));

            Assert.NotNull(elm);

            Assert.Equal(Version01.Value, elm.Version);
            Assert.Equal("111", elm.PharmacyChainCode);

            Assert.Equal(6, elm.Document.Length);

            Assert.Equal("218N129", elm.Document[0].DocNumber);
            Assert.Equal(1, elm.Document[0].CountLines);
            Assert.Single(elm.Document[0].Lines);

            var line = elm.Document[0].Lines[0];
            Assert.Equal("1", line.Line);
            Assert.Equal("1111111111", line.PharmacyInn);
            Assert.Equal("34", line.PharmacyCode);
            Assert.Equal("5408130693", line.DistributorInn);
            Assert.Equal("6102366430032696077", line.CardNumber);
            Assert.Equal("05042011", line.OperDate);
            Assert.Equal(new DateTime(2011, 4, 5), line.OperDateValue);
            Assert.Equal("4034541003578", line.ProductCode);
            Assert.Equal(1, line.Quantity);
            Assert.Equal(1017.12M, line.AmountTotal);
            Assert.Equal(254.28M, line.Discount);
            Assert.Equal(1271.40M, line.Price);
            Assert.Equal(1271.40M, line.PriceMinusNds);
            Assert.Equal(0M, line.AmountNds);
            Assert.Equal(0M, line.DiscountNds);
            Assert.Equal(0, line.PercentNds);
        }
    }
}
