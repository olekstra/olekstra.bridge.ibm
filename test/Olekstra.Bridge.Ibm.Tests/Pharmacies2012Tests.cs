﻿namespace Olekstra.Bridge.Ibm
{
    using System;
    using Xunit;

    public class Pharmacies2012Tests
    {
        [Fact]
        public void PL_522_20121210_121210_1425()
        {
            var elm = SerializationHelper.EL.Deserialize<Pharmacies2012>(ResourceHelper.GetSampleEL("PL_522_20121210_121210_1425.xml"));

            Assert.NotNull(elm);

            Assert.Equal("522", elm.ChainCode);
            Assert.Equal(108, elm.Count);

            Assert.Equal(108, elm.Pharmacy.Length);

            Assert.Equal("9|12|", elm.Pharmacy[0].Code);
            Assert.Equal("5260133971", elm.Pharmacy[0].Inn);
            Assert.Equal($"ООО \"Адонис\"", elm.Pharmacy[0].LegalEntity);
            Assert.Equal("9", elm.Pharmacy[0].Num);
            Assert.Equal("НН, Березовская ул., 83 (Аптека)", elm.Pharmacy[0].Name);
            Assert.Equal("г.Нижний Новгород", elm.Pharmacy[0].City);
            Assert.Equal("г.Н.Новгород, ул.Березовская, 83", elm.Pharmacy[0].Address);
            Assert.Equal(string.Empty, elm.Pharmacy[0].Metro);
            Assert.Equal(string.Empty, elm.Pharmacy[0].ContactPerson);
            Assert.Equal("274-52-00", elm.Pharmacy[0].Phones);
            Assert.Equal(string.Empty, elm.Pharmacy[0].WorkTime);
            Assert.Equal("г.Нижний Новгород г.Н.Новгород, ул.Березовская, 83 НН, Березовская ул., 83 (Аптека)", elm.Pharmacy[0].Description);

            Assert.Equal("На здоровье!", elm.Pharmacy[1].Brand);
        }
    }
}
