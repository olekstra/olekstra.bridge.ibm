﻿namespace Olekstra.Bridge.Ibm
{
    using System.IO;
    using System.Reflection;

    public static class ResourceHelper
    {
        private static Assembly assembly;

        static ResourceHelper()
        {
            assembly = typeof(ResourceHelper).Assembly;
        }

        public static Stream GetSampleEL(string name)
        {
            return assembly.GetManifestResourceStream("Olekstra.Bridge.Ibm.samples.el." + name);
        }
    }
}
