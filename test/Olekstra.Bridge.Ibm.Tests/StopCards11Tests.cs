﻿namespace Olekstra.Bridge.Ibm
{
    using System;
    using System.Text.RegularExpressions;
    using Xunit;

    public class StopCards11Tests
    {
        public static readonly Regex NewLinesAndSpaces = new Regex(@"[\r\n]+\s*");

        [Fact]
        public void Stop_cards_20121217_0503()
        {
            var elm = SerializationHelper.EL.Deserialize<StopCards11>(ResourceHelper.GetSampleEL("stop_cards_20121217_0503.xml"));

            Assert.NotNull(elm);

            Assert.Equal(Version11.Value, elm.Version);
            Assert.Equal("17122012 05:03", elm.ExportDateTime);
            Assert.Equal(new DateTime(2012, 12, 17, 5, 3, 0), elm.ExportDateTimeValue);

            Assert.Equal(448, elm.Cards.Length);
            Assert.Equal("31122010", elm.Cards[0].StopDate);
            Assert.Equal(new DateTime(2010, 12, 31), elm.Cards[0].StopDateValue);
            Assert.Equal("6102366430020453929", elm.Cards[0].CardNumber);
            Assert.Equal("01012025", elm.Cards[0].ReactivationDate);
            Assert.Equal(new DateTime(2025, 1, 1), elm.Cards[0].ReactivationDateValue);
        }

        [Fact]
        public void SerializationOk()
        {
            var elm = new StopCards11
            {
                Version = Version11.Value,
                ExportDateTimeValue = new DateTime(2012, 12, 17, 5, 3, 0),
                Cards = new[]
                {
                    new StopCards_Card
                    {
                        CardNumber = "6102366430020453929",
                        StopDateValue = new DateTime(2010, 12, 31),
                        ReactivationDateValue = new DateTime(2025, 1, 1),
                    },
                },
            };

            var text = SerializationHelper.EL.Serialize(elm);

            var validText = @"
<?xml version=""1.0"" encoding=""windows-1251""?>
<stop_cards>
  <version>1.1</version>
  <export_datetime>17122012 05:03</export_datetime>
  <card>
    <stop_date>31122010</stop_date>
    <card_number>6102366430020453929</card_number>
    <reactivation_date>01012025</reactivation_date>
  </card>
</stop_cards>
";

            Assert.Equal(
                NewLinesAndSpaces.Replace(validText, string.Empty),
                NewLinesAndSpaces.Replace(text, string.Empty));
        }
    }
}
