﻿namespace Olekstra.Bridge.Ibm
{
    using System;
    using System.Text.RegularExpressions;
    using Xunit;

    public class BusinessRules13Tests
    {
        public static readonly Regex NewLinesAndSpaces = StopCards11Tests.NewLinesAndSpaces;

        [Fact]
        public void EL_BR_20121115()
        {
            var elm = SerializationHelper.EL.Deserialize<BusinessRules13>(ResourceHelper.GetSampleEL("EL_BR_20121115.xml"));

            Assert.NotNull(elm);

            Assert.Equal("643", elm.TerritoryCode);
            Assert.Equal(Version13.Value, elm.Version);
            Assert.Equal("15112012", elm.ExportDate);

            Assert.Equal(3, elm.CardType.Length);
            Assert.Equal("001", elm.CardType[0].CardTypeCode);
            Assert.Equal("Cialis", elm.CardType[0].CardTypeDescription);
            Assert.Equal("610294643001049991", elm.CardType[0].CardNumberFrom);
            Assert.Equal("610294643001050000", elm.CardType[0].CardNumberTo);

            Assert.NotNull(elm.CardType[0].Rules);
            Assert.Equal(4, elm.CardType[0].Rules.Length);
            Assert.Equal("1001", elm.CardType[0].Rules[0].SkuRuleId);
            Assert.Equal("01122012", elm.CardType[0].Rules[0].DateFrom);
            Assert.Equal("31122012", elm.CardType[0].Rules[0].DateTo);
            Assert.Equal(new DateTime(2012, 12, 1), elm.CardType[0].Rules[0].DateFromValue);
            Assert.Equal(new DateTime(2012, 12, 31), elm.CardType[0].Rules[0].DateToValue);
            Assert.Equal("4602103003160", elm.CardType[0].Rules[0].SkuCode);
            Assert.Equal("CIALIS 5 MG 1xр14", elm.CardType[0].Rules[0].SkuDescription);
            Assert.Equal(15, elm.CardType[0].Rules[0].Discount);

            Assert.NotNull(elm.DailyReport);
            Assert.Equal(4, elm.DailyReport.Length);
            Assert.Equal("1", elm.DailyReport[0].DailyRuleId);
            Assert.Equal("01122012", elm.DailyReport[0].DateFrom);
            Assert.Equal("31122025", elm.DailyReport[0].DateTo);
            Assert.Equal(new DateTime(2012, 12, 1), elm.DailyReport[0].DateFromValue);
            Assert.Equal(new DateTime(2025, 12, 31), elm.DailyReport[0].DateToValue);
            Assert.Equal("4602103003160", elm.DailyReport[0].SkuCode);
            Assert.Equal("CIALIS 5 MG 1xр14", elm.DailyReport[0].SkuDescription);

            Assert.NotNull(elm.MonthlyReport);
            Assert.Equal(6, elm.MonthlyReport.Length);
            Assert.Equal("1", elm.MonthlyReport[0].MonthlyRuleId);
            Assert.Equal("01122012", elm.MonthlyReport[0].DateFrom);
            Assert.Equal("31122025", elm.MonthlyReport[0].DateTo);
            Assert.Equal(new DateTime(2012, 12, 1), elm.MonthlyReport[0].DateFromValue);
            Assert.Equal(new DateTime(2025, 12, 31), elm.MonthlyReport[0].DateToValue);
            Assert.Equal("4602103003160", elm.MonthlyReport[0].SkuCode);
            Assert.Equal("CIALIS 5 MG 1xр14", elm.MonthlyReport[0].SkuDescription);
        }

        [Fact]
        public void EL_BR_20121214()
        {
            var elm = SerializationHelper.EL.Deserialize<BusinessRules13>(ResourceHelper.GetSampleEL("EL_BR_20121214.xml"));

            Assert.NotNull(elm);

            Assert.Equal("643", elm.TerritoryCode);
            Assert.Equal(Version13.Value, elm.Version);
            Assert.Equal("14122012", elm.ExportDate);

            Assert.Equal(12, elm.CardType.Length);
            Assert.Equal("001", elm.CardType[0].CardTypeCode);
            Assert.Equal("Cialis", elm.CardType[0].CardTypeDescription);
            Assert.Equal("610294643001000001", elm.CardType[0].CardNumberFrom);
            Assert.Equal("610294643001004350", elm.CardType[0].CardNumberTo);

            Assert.NotNull(elm.CardType[0].Rules);
            Assert.Equal(4, elm.CardType[0].Rules.Length);
            Assert.Equal("1001", elm.CardType[0].Rules[0].SkuRuleId);
            Assert.Equal("01012013", elm.CardType[0].Rules[0].DateFrom);
            Assert.Equal("30062013", elm.CardType[0].Rules[0].DateTo);
            Assert.Equal(new DateTime(2013, 1, 1), elm.CardType[0].Rules[0].DateFromValue);
            Assert.Equal(new DateTime(2013, 6, 30), elm.CardType[0].Rules[0].DateToValue);
            Assert.Equal("4602103003160", elm.CardType[0].Rules[0].SkuCode);
            Assert.Equal("CIALIS 5 MG 1xр14", elm.CardType[0].Rules[0].SkuDescription);
            Assert.Equal(15, elm.CardType[0].Rules[0].Discount);

            Assert.NotNull(elm.DailyReport);
            Assert.Equal(4, elm.DailyReport.Length);
            Assert.Equal("1", elm.DailyReport[0].DailyRuleId);
            Assert.Equal("01012013", elm.DailyReport[0].DateFrom);
            Assert.Equal("31122025", elm.DailyReport[0].DateTo);
            Assert.Equal(new DateTime(2013, 1, 1), elm.DailyReport[0].DateFromValue);
            Assert.Equal(new DateTime(2025, 12, 31), elm.DailyReport[0].DateToValue);
            Assert.Equal("4602103003160", elm.DailyReport[0].SkuCode);
            Assert.Equal("CIALIS 5 MG 1xр14", elm.DailyReport[0].SkuDescription);

            Assert.NotNull(elm.MonthlyReport);
            Assert.Equal(6, elm.MonthlyReport.Length);
            Assert.Equal("1", elm.MonthlyReport[0].MonthlyRuleId);
            Assert.Equal("01012013", elm.MonthlyReport[0].DateFrom);
            Assert.Equal("31122025", elm.MonthlyReport[0].DateTo);
            Assert.Equal(new DateTime(2013, 1, 1), elm.MonthlyReport[0].DateFromValue);
            Assert.Equal(new DateTime(2025, 12, 31), elm.MonthlyReport[0].DateToValue);
            Assert.Equal("4602103003160", elm.MonthlyReport[0].SkuCode);
            Assert.Equal("CIALIS 5 MG 1xр14", elm.MonthlyReport[0].SkuDescription);
        }

        [Fact]
        public void SerializationOk()
        {
            var elm = new BusinessRules13
            {
                TerritoryCode = "643",
                Version = Version13.Value,
                ExportDateValue = new DateTime(2012, 12, 14),
                CardType = new[]
                {
                    new BusinessRules_CardType
                    {
                        CardTypeCode = "10001",
                        CardTypeDescription = "Cards!",
                        CardNumberFrom = "123456789012",
                        CardNumberTo = "123456789099",
                        Rules = new[]
                        {
                            new BusinessRules_CardType_SkuRule
                            {
                                SkuRuleId = "123",
                                SkuCode = "SKU1",
                                SkuDescription = "Sku 1",
                                Discount = 10,
                                DateFromValue = new DateTime(2011, 1, 2),
                                DateToValue = new DateTime(2025, 12, 31),
                            }
                        }
                    }
                },
                DailyReport = new[]
                {
                    new BusinessRules_DailyRule
                    {
                        DailyRuleId = "10001",
                        DateFromValue = new DateTime(2013, 1, 2),
                        DateToValue = new DateTime(2025, 12, 31),
                        SkuCode = "1234567890123",
                        SkuDescription = "Some SKU",
                    },
                },
                MonthlyReport = new[]
                {
                    new BusinessRules_MonthlyRule
                    {
                        MonthlyRuleId = "10001",
                        DateFromValue = new DateTime(2013, 1, 2),
                        DateToValue = new DateTime(2025, 12, 31),
                        SkuCode = "1234567890123",
                        SkuDescription = "Some SKU",
                    },
                },
            };

            var text = SerializationHelper.EL.Serialize(elm);

            var validText = @"
<?xml version=""1.0"" encoding=""windows-1251""?>
<business_rules>
  <territory_code>643</territory_code>
  <version>1.3</version>
  <export_date>14122012</export_date>
  <card_type>
    <card_type_code>10001</card_type_code>
    <card_type_description>Cards!</card_type_description>
    <card_number_from>123456789012</card_number_from>
    <card_number_to>123456789099</card_number_to>
    <rules>
      <sku_rule>
        <sku_rule_id>123</sku_rule_id>
        <date_from>02012011</date_from>
        <date_to>31122025</date_to>
        <sku_code>SKU1</sku_code>
        <sku_description>Sku 1</sku_description>
        <discount>10</discount>
      </sku_rule>
    </rules>
  </card_type>
  <daily_report>
    <daily_rule>
      <daily_rule_id>10001</daily_rule_id>
      <date_from>02012013</date_from>
      <date_to>31122025</date_to>
      <sku_code>1234567890123</sku_code>
      <sku_description>Some SKU</sku_description>
    </daily_rule>
  </daily_report>
  <monthly_report>
    <monthly_rule>
      <monthly_rule_id>10001</monthly_rule_id>
      <date_from>02012013</date_from>
      <date_to>31122025</date_to>
      <sku_code>1234567890123</sku_code>
      <sku_description>Some SKU</sku_description>
    </monthly_rule>
  </monthly_report>
</business_rules>
";

            Assert.Equal(
                NewLinesAndSpaces.Replace(validText, string.Empty),
                NewLinesAndSpaces.Replace(text, string.Empty));
        }
    }
}
