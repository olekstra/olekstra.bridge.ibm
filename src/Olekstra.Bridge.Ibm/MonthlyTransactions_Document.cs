﻿namespace Olekstra.Bridge.Ibm
{
    using System;
    using System.Xml.Serialization;

    [XmlType(AnonymousType = true)]
    public class MonthlyTransactions_Document
    {
        [XmlElement("DOCNUMBER")]
        public string DocNumber { get; set; }

        [XmlElement("COUNTLINES")]
        public int CountLines { get; set; }

        [XmlArray("LINES")]
        [XmlArrayItem("DOCLINE", IsNullable = false)]
        public MonthlyTransactions_DocLine[] Lines { get; set; }
    }
}
