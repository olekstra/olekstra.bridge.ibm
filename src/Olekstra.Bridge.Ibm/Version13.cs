﻿namespace Olekstra.Bridge.Ibm
{
    using System;
    using System.Xml.Serialization;

    [XmlType(TypeName = "version-1-3")]
    public enum Version13
    {
        [XmlEnum("1.3")]
        Value,
    }
}
