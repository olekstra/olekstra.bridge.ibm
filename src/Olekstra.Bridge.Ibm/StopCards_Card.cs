﻿namespace Olekstra.Bridge.Ibm
{
    using System;
    using System.Globalization;
    using System.Xml.Serialization;

    public class StopCards_Card
    {
        [XmlElement("stop_date")]
        public string StopDate { get; set; }

        [XmlElement("card_number")]
        public string CardNumber { get; set; }

        [XmlElement("reactivation_date")]
        public string ReactivationDate { get; set; }

        /// <summary>
        /// DateTime value of <see cref="StopDate"/>
        /// </summary>
        [XmlIgnore]
        public DateTime StopDateValue
        {
            get
            {
                return DateTime.ParseExact(StopDate, "ddMMyyyy", CultureInfo.InvariantCulture);
            }

            set
            {
                StopDate = value.ToString("ddMMyyyy", CultureInfo.InvariantCulture);
            }
        }

        /// <summary>
        /// DateTime value of <see cref="ReactivationDate"/>
        /// </summary>
        [XmlIgnore]
        public DateTime ReactivationDateValue
        {
            get
            {
                return DateTime.ParseExact(ReactivationDate, "ddMMyyyy", CultureInfo.InvariantCulture);
            }

            set
            {
                ReactivationDate = value.ToString("ddMMyyyy", CultureInfo.InvariantCulture);
            }
        }
    }
}
