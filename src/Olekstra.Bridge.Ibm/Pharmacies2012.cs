﻿namespace Olekstra.Bridge.Ibm
{
    using System;
    using System.Xml.Serialization;

    [XmlRoot("PHARMACIES", Namespace = "", IsNullable = false)]
    public class Pharmacies2012 : Pharmacies
    {
        [XmlElement("CHAINCODE")]
        public string ChainCode { get; set; }

        [XmlElement("COUNT")]
        public int Count { get; set; }

        [XmlElement("PHARMACY")]
        public Pharmacies_Pharmacy[] Pharmacy { get; set; }
    }
}
