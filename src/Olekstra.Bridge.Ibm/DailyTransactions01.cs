﻿namespace Olekstra.Bridge.Ibm
{
    using System;
    using System.Xml.Serialization;

    [XmlRoot("DAILYTRANSACTIONS", Namespace = "", IsNullable = false)]
    public class DailyTransactions01 : DailyTransactions
    {
        [XmlElement("VERSION")]
        public Version01 Version { get; set; }

        [XmlElement("PHARMACYCHAINCODE", DataType = "positiveInteger")]
        public string PharmacyChainCode { get; set; }

        [XmlElement("DOCUMENT")]
        public DailyTransactions_Document[] Document { get; set; }
    }
}
