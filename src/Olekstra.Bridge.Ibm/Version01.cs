﻿namespace Olekstra.Bridge.Ibm
{
    using System;
    using System.Xml.Serialization;

    [XmlType(TypeName = "version-0-1")]
    public enum Version01
    {
        [XmlEnum("0.1")]
        Value,
    }
}
