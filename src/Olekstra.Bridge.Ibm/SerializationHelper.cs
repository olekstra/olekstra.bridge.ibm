﻿namespace Olekstra.Bridge.Ibm
{
    using System;
    using System.IO;
    using System.Text;
    using System.Xml;
    using System.Xml.Linq;
    using System.Xml.Schema;
    using System.Xml.Serialization;

    public abstract class SerializationHelper
    {
        private static readonly Lazy<SerializationHelper> LazySerializationHelperEL
            = new Lazy<SerializationHelper>(() => new SerializationHelperEL());

        private readonly XmlSchemaSet schemas;

        private bool additionalEncodingsRegistered = false;

        private Encoding windowsEncoding = null;

        protected SerializationHelper()
        {
            this.schemas = GetSchemas();
        }

        public static SerializationHelper EL
        {
            get { return LazySerializationHelperEL.Value; }
        }

        public static SerializationHelper Create(ProtocolVersion protocolVersion)
        {
            switch (protocolVersion)
            {
                case ProtocolVersion.EL:
                    return LazySerializationHelperEL.Value;
                default:
                    throw new ArgumentOutOfRangeException(nameof(protocolVersion));
            }
        }

        public void RegisterAdditionalEncodings()
        {
            if (!additionalEncodingsRegistered)
            {
                Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
                windowsEncoding = Encoding.GetEncoding("windows-1251");
                additionalEncodingsRegistered = true;
            }
        }

        public XDocument Load(string xml)
        {
            RegisterAdditionalEncodings();

            using (var sr = new StringReader(xml))
            {
                var doc = XDocument.Load(new StringReader(xml));
                doc.Validate(schemas, null); // throws exception when validation error occurs
                return doc;
            }
        }

        public XDocument Load(Stream xml)
        {
            RegisterAdditionalEncodings();

            var doc = XDocument.Load(xml);
            doc.Validate(schemas, null); // throws exception when validation error occurs
            return doc;
        }

        public TElement Deserialize<TElement>(byte[] xml)
        {
            using (var ms = new MemoryStream(xml))
            {
                var doc = Load(ms);
                var serializer = new System.Xml.Serialization.XmlSerializer(typeof(TElement));
                return (TElement)serializer.Deserialize(doc.CreateReader());
            }
        }

        public TElement Deserialize<TElement>(Stream xml)
        {
            var doc = Load(xml);
            var serializer = new System.Xml.Serialization.XmlSerializer(typeof(TElement));
            return (TElement)serializer.Deserialize(doc.CreateReader());
        }

        public TElement Deserialize<TElement>(string xml)
        {
            var doc = Load(xml);
            var serializer = new System.Xml.Serialization.XmlSerializer(typeof(TElement));
            return (TElement)serializer.Deserialize(doc.CreateReader());
        }

        public string Serialize(object element)
        {
            if (element == null)
            {
                throw new ArgumentNullException(nameof(element));
            }

            using (var ms = SerializeToStream(element))
            {
                using (var sr = new StreamReader(ms))
                {
                    return sr.ReadToEnd();
                }
            }
        }

        public MemoryStream SerializeToStream(object element)
        {
            if (element == null)
            {
                throw new ArgumentNullException(nameof(element));
            }

            RegisterAdditionalEncodings();

            var ms = new MemoryStream();
            using (var sw = new StreamWriter(ms, windowsEncoding, 2048, true))
            {
                var ns = new XmlSerializerNamespaces();
                ns.Add(string.Empty, string.Empty);
                var serializer = new XmlSerializer(element.GetType());
                serializer.Serialize(sw, element, ns);
            }

            ms.Position = 0;
            return ms;
        }

        public byte[] SerializeToByteArray(object element)
        {
            if (element == null)
            {
                throw new ArgumentNullException(nameof(element));
            }

            using (var ms = SerializeToStream(element))
            {
                return ms.ToArray();
            }
        }

        protected abstract XmlSchemaSet GetSchemas();
    }
}
