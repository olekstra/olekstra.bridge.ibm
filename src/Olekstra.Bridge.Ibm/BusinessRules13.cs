﻿namespace Olekstra.Bridge.Ibm
{
    using System;
    using System.Globalization;
    using System.Xml.Serialization;

    [XmlRoot("business_rules", Namespace = "", IsNullable = false)]
    public class BusinessRules13 : BusinessRules
    {
        [XmlElement("territory_code")]
        public string TerritoryCode { get; set; }

        [XmlElement("version")]
        public Version13 Version { get; set; }

        [XmlElement("export_date")]
        public string ExportDate { get; set; }

        /// <summary>
        /// DateTime value of <see cref="ExportDate"/>
        /// </summary>
        [XmlIgnore]
        public DateTime ExportDateValue
        {
            get
            {
                return DateTime.ParseExact(ExportDate, "ddMMyyyy", CultureInfo.InvariantCulture);
            }

            set
            {
                ExportDate = value.ToString("ddMMyyyy", CultureInfo.InvariantCulture);
            }
        }

        [XmlElement("card_type")]
        public BusinessRules_CardType[] CardType { get; set; }

        [XmlArray("daily_report")]
        [XmlArrayItem("daily_rule", IsNullable = false)]
        public BusinessRules_DailyRule[] DailyReport { get; set; }

        [XmlArray("monthly_report")]
        [XmlArrayItem("monthly_rule", IsNullable = false)]
        public BusinessRules_MonthlyRule[] MonthlyReport { get; set; }
    }
}
