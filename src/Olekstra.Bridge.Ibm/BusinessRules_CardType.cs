﻿namespace Olekstra.Bridge.Ibm
{
    using System;
    using System.Xml.Serialization;

    [XmlType(AnonymousType = true)]
    public class BusinessRules_CardType
    {
        [XmlElement("card_type_code")]
        public string CardTypeCode { get; set; }

        [XmlElement("card_type_description")]
        public string CardTypeDescription { get; set; }

        [XmlElement("card_number_from")]
        public string CardNumberFrom { get; set; }

        [XmlElement("card_number_to")]
        public string CardNumberTo { get; set; }

        [XmlArray("rules")]
        [XmlArrayItem("sku_rule", IsNullable = false)]
        public BusinessRules_CardType_SkuRule[] Rules { get; set; }
    }
}
