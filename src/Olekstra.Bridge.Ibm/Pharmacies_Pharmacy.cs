﻿namespace Olekstra.Bridge.Ibm
{
    using System;
    using System.Xml.Serialization;

    [XmlType(AnonymousType = true)]
    public class Pharmacies_Pharmacy
    {
        /// <summary>
        /// Уникальный внутренний код аптеки
        /// </summary>
        [XmlElement("CODE")]
        public string Code { get; set; }

        /// <summary>
        /// ИНН юридического лица – владельца аптеки, в которой была осуществлена продажа
        /// </summary>
        [XmlElement("INN")]
        public string Inn { get; set; }

        /// <summary>
        /// Наименование юридического лица аптеки
        /// </summary>
        [XmlElement("LEGALENTITY")]
        public string LegalEntity { get; set; }

        /// <summary>
        /// Торговая марка аптеки
        /// </summary>
        [XmlElement("BRAND")]
        public string Brand { get; set; }

        /// <summary>
        /// Номер аптеки
        /// </summary>
        [XmlElement("NUM")]
        public string Num { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        [XmlElement("NAME")]
        public string Name { get; set; }

        /// <summary>
        /// Город
        /// </summary>
        [XmlElement("CITY")]
        public string City { get; set; }

        /// <summary>
        /// Адрес
        /// </summary>
        [XmlElement("ADDRESS")]
        public string Address { get; set; }

        /// <summary>
        /// Метро
        /// </summary>
        [XmlElement("METRO")]
        public string Metro { get; set; }

        /// <summary>
        /// Контактное лицо
        /// </summary>
        [XmlElement("CONTACTPERSON")]
        public string ContactPerson { get; set; }

        /// <summary>
        /// Телефоны
        /// </summary>
        [XmlElement("PHONES")]
        public string Phones { get; set; }

        /// <summary>
        /// Время работы
        /// </summary>
        [XmlElement("WORKTIME")]
        public string WorkTime { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        [XmlElement("DESCRIPTION")]
        public string Description { get; set; }
    }
}
