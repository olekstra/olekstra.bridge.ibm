﻿namespace Olekstra.Bridge.Ibm
{
    using System;
    using System.Globalization;
    using System.Xml.Serialization;

    public class DailyTransactions_DocLine
    {
        [XmlElement("LINE")]
        public string Line { get; set; }

        [XmlElement("PHARMACYINN")]
        public string PharmacyInn { get; set; }

        [XmlElement("PHARMACYCODE")]
        public string PharmacyCode { get; set; }

        [XmlElement("CARDNUMBER")]
        public string CardNumber { get; set; }

        [XmlElement("OPERDATE")]
        public string OperDate { get; set; }

        /// <summary>
        /// DateTime value of <see cref="OperDate"/>
        /// </summary>
        public DateTime OperDateValue
        {
            get
            {
                return DateTime.ParseExact(OperDate, "ddMMyyyy", CultureInfo.InvariantCulture);
            }

            set
            {
                OperDate = value.ToString("ddMMyyyy", CultureInfo.InvariantCulture);
            }
        }

        [XmlElement("CASHREGISTERNUMBER")]
        public string CashRegisterNumber { get; set; }

        [XmlElement("SHIFTNUMBER")]
        public string ShiftNumber { get; set; }

        [XmlElement("PRODUCTCODE")]
        public string ProductCode { get; set; }

        [XmlElement("QUANTITY")]
        public int Quantity { get; set; }

        [XmlElement("AMOUNTTOTAL")]
        public decimal AmountTotal { get; set; }

        [XmlElement("DISCOUNT")]
        public decimal Discount { get; set; }

        [XmlElement("PRICE")]
        public decimal Price { get; set; }
    }
}
