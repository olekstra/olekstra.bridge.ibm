﻿namespace Olekstra.Bridge.Ibm
{
    using System;
    using System.Xml;
    using System.Xml.Schema;

    public class SerializationHelperEL : SerializationHelper
    {
        protected override XmlSchemaSet GetSchemas()
        {
            var assembly = typeof(SerializationHelper).Assembly;

            var schemas = new XmlSchemaSet();
            using (var sr = assembly.GetManifestResourceStream("Olekstra.Bridge.Ibm.common_types.xsd"))
            {
                schemas.Add(string.Empty, XmlReader.Create(sr));
            }

            using (var sr = assembly.GetManifestResourceStream("Olekstra.Bridge.Ibm.schema_el.xsd"))
            {
                schemas.Add(string.Empty, XmlReader.Create(sr));
            }

            return schemas;
        }
    }
}
