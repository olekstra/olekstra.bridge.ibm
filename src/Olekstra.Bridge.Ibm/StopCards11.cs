﻿namespace Olekstra.Bridge.Ibm
{
    using System;
    using System.Globalization;
    using System.Xml.Serialization;

    [XmlRoot("stop_cards", Namespace = "", IsNullable = false)]
    public class StopCards11 : StopCards
    {
        [XmlElement("version")]
        public Version11 Version { get; set; }

        [XmlElement("export_datetime")]
        public string ExportDateTime { get; set; }

        /// <summary>
        /// DateTime value of <see cref="ExportDateTime"/>
        /// </summary>
        [XmlIgnore]
        public DateTime ExportDateTimeValue
        {
            get
            {
                return DateTime.ParseExact(ExportDateTime, "ddMMyyyy HH:mm", CultureInfo.InvariantCulture);
            }

            set
            {
                ExportDateTime = value.ToString("ddMMyyyy HH:mm", CultureInfo.InvariantCulture);
            }
        }

        [XmlElement("card")]
        public StopCards_Card[] Cards { get; set; }
    }
}
