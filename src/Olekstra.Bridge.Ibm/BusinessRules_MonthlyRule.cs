﻿namespace Olekstra.Bridge.Ibm
{
    using System;
    using System.Globalization;
    using System.Xml.Serialization;

    [XmlType(AnonymousType = true)]
    public class BusinessRules_MonthlyRule
    {
        [XmlElement("monthly_rule_id", DataType = "positiveInteger")]
        public string MonthlyRuleId { get; set; }

        [XmlElement("date_from")]
        public string DateFrom { get; set; }

        [XmlElement("date_to")]
        public string DateTo { get; set; }

        /// <summary>
        /// DateTime value of <see cref="DateFrom"/>
        /// </summary>
        [XmlIgnore]
        public DateTime DateFromValue
        {
            get
            {
                return DateTime.ParseExact(DateFrom, "ddMMyyyy", CultureInfo.InvariantCulture);
            }

            set
            {
                DateFrom = value.ToString("ddMMyyyy", CultureInfo.InvariantCulture);
            }
        }

        /// <summary>
        /// DateTime value of <see cref="DateTo"/>
        /// </summary>
        [XmlIgnore]
        public DateTime DateToValue
        {
            get
            {
                return DateTime.ParseExact(DateTo, "ddMMyyyy", CultureInfo.InvariantCulture);
            }

            set
            {
                DateTo = value.ToString("ddMMyyyy", CultureInfo.InvariantCulture);
            }
        }

        [XmlElement("sku_code")]
        public string SkuCode { get; set; }

        [XmlElement("sku_description")]
        public string SkuDescription { get; set; }
    }
}
