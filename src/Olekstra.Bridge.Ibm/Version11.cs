﻿namespace Olekstra.Bridge.Ibm
{
    using System;
    using System.Xml.Serialization;

    [XmlType(TypeName = "version-1-1")]
    public enum Version11
    {
        [XmlEnum("1.1")]
        Value,
    }
}
