﻿namespace Olekstra.Bridge.Ibm
{
    using System;
    using System.Xml.Serialization;

    [XmlRoot("MONTHLYTRANSACTIONS", Namespace = "", IsNullable = false)]
    public class MonthlyTransactions01 : MonthlyTransactions
    {
        [XmlElement("VERSION")]
        public Version01 Version { get; set; }

        [XmlElement("PHARMACYCHAINCODE", DataType = "positiveInteger")]
        public string PharmacyChainCode { get; set; }

        [XmlElement("DOCUMENT")]
        public MonthlyTransactions_Document[] Document { get; set; }
    }
}
